from io import BytesIO

from fastapi import HTTPException
from PIL import Image
from wsi_service.models.v3.slide import SlideExtent, SlideInfo, SlideLevel, SlidePixelSizeNm
from wsi_service.singletons import settings
from wsi_service.slide import Slide as BaseSlide
from wsi_service.utils.slide_utils import get_rgb_channel_list

from wsi_service_plugin_isyntax.zmq_connection import ZmqConnection


class Slide(BaseSlide):
    async def open(self, filepath):
        self.filepath = filepath

        # we need to remove the local data dir from our filename because the local
        # dir is mapped to /data in isyntax-backend container
        if filepath.startswith("/data"):
            self.filepath = filepath.replace("/data", "")

        zmq_connection = ZmqConnection()
        req = {"req": "verification", "filepath": self.filepath}

        rep_msg = await zmq_connection.send_and_recv_json(req)
        if rep_msg["status_code"] != 200:
            raise HTTPException(
                status_code=rep_msg["status_code"],
                detail=rep_msg["detail"],
            )
        self.slide_info = await self.__get_info()
        self.thumbnail = await self.get_thumbnail(settings.max_thumbnail_size, settings.max_thumbnail_size)

    async def close(self):
        self.filepath = None

    async def get_info(self):
        return self.slide_info

    async def __get_info(self):
        zmq_connection = ZmqConnection()
        req = {"req": "get_info", "filepath": self.filepath}
        rep_msg = await zmq_connection.send_and_recv_json(req)
        return self.__parse_info_reply(rep_msg)

    async def get_region(self, level, start_x, start_y, size_x, size_y, padding_color=None, z=0):
        zmq_connection = ZmqConnection()
        req = {
            "req": "get_region",
            "filepath": self.filepath,
            "level": level,
            "start_x": start_x,
            "start_y": start_y,
            "size_x": size_x,
            "size_y": size_y,
        }

        data, image_array = await zmq_connection.send_and_recv_multi(req)
        if data["rep"] == "success":
            image = Image.frombuffer("RGB", (data["width"], data["height"]), image_array, "raw", "RGB", 0, 1)
            image = image.resize((data["width"] - 1, data["height"] - 1), Image.ANTIALIAS)
            return image
        else:
            raise HTTPException(status_code=data["status_code"], detail=data["details"])

    async def get_thumbnail(self, max_x, max_y):
        zmq_connection = ZmqConnection()
        req = {
            "req": "get_thumbnail",
            "filepath": self.filepath,
            "max_x": max_x,
            "max_y": max_y,
        }
        data, image_array = await zmq_connection.send_and_recv_multi(req)
        if data["rep"] == "success":
            image = Image.frombuffer("RGB", (data["width"], data["height"]), image_array, "raw", "RGB", 0, 1)
            image.thumbnail((max_x, max_y), resample=Image.ANTIALIAS)
            return image
        else:
            raise HTTPException(status_code=data["status_code"], detail=data["details"])

    async def _get_associated_image(self, associated_image_name):
        zmq_connection = ZmqConnection()
        req = {"req": associated_image_name, "filepath": self.filepath}
        image_data = await zmq_connection.send_and_recv(req)
        return Image.open(BytesIO(image_data))

    async def get_label(self):
        return await self._get_associated_image("LABEL")

    async def get_macro(self):
        return await self._get_associated_image("MACRO")

    async def get_tile(self, level, tile_x, tile_y, padding_color=None, z=0):
        zmq_connection = ZmqConnection()
        req = {
            "req": "get_tile",
            "filepath": self.filepath,
            "level": level,
            "tile_x": tile_x,
            "tile_y": tile_y,
        }
        data, image_array = await zmq_connection.send_and_recv_multi(req)
        if data["rep"] == "success":
            image = Image.frombuffer("RGB", (data["width"], data["height"]), image_array, "raw", "RGB", 0, 1)
            image = image.resize((data["width"] - 1, data["height"] - 1), Image.ANTIALIAS)
            return image
        else:
            raise HTTPException(status_code=data["status_code"], detail=data["details"])

    # private member
    def __parse_levels(self, info):
        levels = []
        for level in info["levels"]:
            levels.append(
                SlideLevel(
                    extent=SlideExtent(
                        x=level["extent"]["x"],
                        y=level["extent"]["y"],
                        z=level["extent"]["z"],
                    ),
                    downsample_factor=level["downsample_factor"],
                )
            )
        return levels

    def __parse_info_reply(self, info):
        levels = self.__parse_levels(info)
        slide_info_obj = SlideInfo(
            id=info["id"],
            channels=get_rgb_channel_list(),  # rgb channels
            channel_depth=8,  # 8bit each channel
            extent=SlideExtent(x=info["extent"]["x"], y=info["extent"]["y"], z=info["extent"]["z"]),
            pixel_size_nm=SlidePixelSizeNm(x=info["pixel_size_nm"][0], y=info["pixel_size_nm"][1], z=0),
            tile_extent=SlideExtent(
                x=info["tile_extent"]["x"],
                y=info["tile_extent"]["y"],
                z=info["tile_extent"]["z"],
            ),
            num_levels=len(levels),
            levels=levels,
        )
        return slide_info_obj
