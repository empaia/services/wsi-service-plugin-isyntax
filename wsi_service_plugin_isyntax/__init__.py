import os

from .slide import Slide

priority = 1


def is_supported(filepath):
    if os.path.isfile(filepath):
        return filepath.endswith(".isyntax")
    return False


async def open(filepath):
    return await Slide.create(filepath)
