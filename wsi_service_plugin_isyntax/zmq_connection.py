import uuid
from typing import Dict, List

import zmq
from fastapi import HTTPException
from zmq.asyncio import Context, Poller, Socket


class ZmqConnection:
    port = 5556

    def __init__(self):
        self.client_id = uuid.uuid4()
        self.context: Context = Context()
        self.socket: Socket = self.context.socket(zmq.DEALER)
        identity = "client-%d" % self.client_id
        self.socket.identity = identity.encode("ascii")
        self.socket.connect(f"tcp://isyntax-backend:{self.port}")

        self.poller: Poller = Poller()
        self.poller.register(self.socket, zmq.POLLIN)

    async def send_and_recv_json(self, req_msg: Dict) -> Dict:
        self.socket.send_json(req_msg)

        if await self.poller.poll():
            rep_msg = await self.socket.recv_json()
        else:
            raise HTTPException(
                status_code=408,
                detail="Pyzmq request timeout. Could not reach isyntax backend.",
            )
        self.socket.close()
        self.context.term()
        return rep_msg

    async def send_and_recv_multi(self, req_msg: Dict) -> (Dict, List):
        self.socket.send_json(req_msg)
        if await self.poller.poll():
            data = await self.socket.recv_json()
            image_array = await self.socket.recv()
        else:
            raise HTTPException(
                status_code=408,
                detail="Pyzmq request timeout. Could not reach isyntax backend.",
            )
        self.socket.close()
        self.context.term()
        return data, image_array

    async def send_and_recv(self, req_msg: Dict):
        self.socket.send_json(req_msg)
        if await self.poller.poll():
            return await self.socket.recv()
