FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.2.8@sha256:0c20dd487c2bb040fd78991bf54f396cb1fd9ab314a7a55bee0ad4909748797d AS plugin_build

COPY . /plugin_build
WORKDIR /plugin_build

RUN poetry build

RUN poetry export --dev --without-hashes --output ./dist/dev-requirements.txt


FROM registry.gitlab.com/empaia/services/wsi-service:0.12.6@sha256:14c6c109002c84fc4846c16e09404110a9e5374ad24fca89952cb40a209f98b0 AS wsi_service_development_plugin_integration

COPY --from=plugin_build /plugin_build/dist/ /plugin_build/dist/

RUN pip3 install --user --no-warn-script-location -r /plugin_build/dist/dev-requirements.txt

RUN pip3 install --user --no-warn-script-location /plugin_build/dist/*.whl


FROM registry.gitlab.com/empaia/services/wsi-service:0.12.6@sha256:14c6c109002c84fc4846c16e09404110a9e5374ad24fca89952cb40a209f98b0 AS wsi_service_production_plugin_production

COPY --from=plugin_build /plugin_build/dist/ /plugin_build/dist/

RUN pip3 install /plugin_build/dist/*.whl
