# wsi-service-plugin-isyntax

## Requirements

For `isyntax`-support register and download the SDK for free on the [Philips Pathology SDK Site](https://www.usa.philips.com/healthcare/sites/pathology/about/sdk) (Note: Make sure to download version for _Ubuntu 18.04_ and _Python 3.6.9_).

Download `philips-pathologysdk-2.0-ubuntu18_04_py36_research.zip` to `isyntax_backend/philips-pathologysdk-2.0-ubuntu18_04_py36_research.zip`.

## Get started

Make sure [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/) is installed.

Set environment variables in your shell or in a `.env` file:

```bash
WS_CORS_ALLOW_ORIGINS=["*"]
WS_DISABLE_OPENAPI=False
WS_MAPPER_ADDRESS=http://localhost:8080/slides/{slide_id}/storage
WS_LOCAL_MODE=True
WS_INACTIVE_HISTO_IMAGE_TIMEOUT_SECONDS=600
WS_MAX_RETURNED_REGION_SIZE=25000000
WS_ROOT_PATH=
WS_ENABLE_VIEWER_ROUTES=False

COMPOSE_RESTART=no
COMPOSE_NETWORK=default
COMPOSE_WS_PORT=8080
COMPOSE_DATA_DIR=/data
```

See  WSI Service repository for details on these variables

```bash
docker-compose up --build
```

Afterwards, visit `http://localhost:${COMPOSE_WS_PORT}/docs` (Note: Running on port that is defined as environment variable)

## Development

```bash
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build 
```

### Run tests

Run while development composition is up and running:

```bash
docker exec -it wsi-service-plugin-isyntax_wsi_service_1 sh -c "cd /home/appuser/.local/lib/python3.8/site-packages/wsi_service_plugin_isyntax/ && python3 -m pytest"
```

See  WSI Service repository for more details on testdata and debugging.
