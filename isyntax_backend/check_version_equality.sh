#!/bin/bash
if [[ "$1" == "$2" ]]; then
  exit 0
else
  echo "Plugin version [$1] does not match backend version [$2]"
  exit 1
fi