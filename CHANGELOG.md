# Changelog

## 0.7.9

- renovate

## 0.7.8

- renovate

## 0.7.7

- renovate
- fixed pillow < 10.0.0

## 0.7.6

- renovate

## 0.7.5

- reworked isyntax backend to handle one slide by one worker
- cache slide workers and clean up after expiration time

## 0.7.3 & 0.7.4

- renovate

## 0.7.2

- bugfix for is_supported function

## 0.7.1

- renovate

## 0.7.0

- support new plugin mechanism of wsi-service


## 0.6.11

- renovate

## 0.6.10

- renovate

## 0.6.9

- renovate

## 0.6.8

- renovate

## 0.6.7

- renovate

## 0.6.6

- renovate

## 0.6.5

- renovate

## 0.6.4

- renovate

## 0.6.3

- renovate

## 0.6.2

- renovate

## 0.6.1

- Fixed vscode debug path

## 0.6.0

- Update WSI Service to version 0.10.0

## 0.5.7

- renovate

## 0.5.6

- renovate

## 0.5.5

- renovate

## 0.5.4

- renovate

## 0.5.3

- renovate

## 0.5.2

- renovate

## 0.5.1

- renovate

## 0.5.0

- Update WSI Service to version 0.9.2

## 0.4.27

- renovate

## 0.4.26

- renovate

## 0.4.25

- renovate

## 0.4.24

- renovate

## 0.4.23

- renovate

## 0.4.22

- renovate

## 0.4.21

- renovate

## 0.4.20

- renovate

## 0.4.19

- renovate

## 0.4.18

- renovate

## 0.4.17

- renovate

## 0.4.16

- renovate

## 0.4.15

- renovate

## 0.4.14

- renovate

## 0.4.13

- renovate

## 0.4.12

- renovate

## 0.4.11

- renovate

## 0.4.10

- renovate

## 0.4.9

- renovate

## 0.4.8

- renovate

## 0.4.7

- renovate

## 0.4.6

- updated dependencies

## 0.4.5

- updated dependencies

## 0.4.4

- fix ci build

## 0.4.3

- fix base-image

## 0.4.2

- Updated CI

## 0.4.1

- Updated CI
- Updated WSI Service to 0.7.3

## 0.4.0

- Added async backend with multithreading
- Updated WSI Service to 0.6.5

## 0.3.1

- Matching version of plugin and isyntax_backend

## 0.1.2

- Updated WSI Service to 0.6.2
- Made all plugin calls async

## 0.1.1

- Updated WSI Service to 0.5.3
- open() call now async
